# Griffin Moran
# Oct 8, 2015
# TriviaGame


class Question:
    def __init__(self, question, answer_1, answer_2, answer_3, answer_4, correct_answer):

        self.__question = question
        self.__answer_1 = answer_1
        self.__answer_2 = answer_2
        self.__answer_3 = answer_3
        self.__answer_4 = answer_4
        self.__correct_answer = correct_answer

    def ask(self):
        print(self.__question)
        print("1. " + self.__answer_1)
        print("2. " + self.__answer_2)
        print("3. " + self.__answer_3)
        print("4. " + self.__answer_4)

    def correct(self, response, player1):

        if response == self.__correct_answer:

            return True
        else:

            return False

    def getCorrect(self):
        print
