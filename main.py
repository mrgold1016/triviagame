# Griffin Moran
# Oct 8, 2015
# TriviaGame

import Question
'''
file = open("newfile.txt", "w")

file.write("hello world in the new file\n")

file.write("and another line\n")

file.close()

file = open('newfile.txt', 'r')

print(file.read())
'''
Questions = ["How many licks does it take to get to the center of a Tootsie Pop™ ?",
             "What does the scouter say about his power level", "Which video game giant is the worst?",
             "What company made the Nintendo™ 3DS's Miiverse minigames?",
             "Who co-developed Nintendo's™ The Legend Of Zelda: Ocarina of Time?"]

answer1 = ["10", "Over 9000", "Konami", "Nintendo", "Sega"]
answer2 = ["20", "1000", "Ubisoft", "PROPE", "Nintendo"]
answer3 = ["30", "10000", "Electronic Arts", "Valve", "Grezzo"]
answer4 = ["The world may never know", "5000", "Valve", "HAL Laboratory", "PROPE"]
correct = [4, 1, 1, 2, 3]
questionList = list()
player1 = 0
player2 = 0
x = 0
turns = 5

while x < turns:
    q = Question.Question(Questions[x], answer1[x], answer2[x], answer3[x], answer4[x], correct[x])
    questionList.append(q)
    x += 1

turn = 0
while turn < turns:

    if turn % 2 != 0:
        print("Player 2 Answer ")
        questionList[turn].ask()
        response = int(input("Enter a number"))
        questionList[turn].getCorrect()

        if questionList[turn].getCorrect():
            player2 += 1


    else:

        questionList[turn].ask()
        response = int(input("Enter a number"))
        questionList[turn].getCorrect()

        if questionList[turn].getCorrect():
            player1 += 1
    turn += 1

    if questionList[turn].correct(response, player1):
        print("Nice job. You got it right")
    else:
        print("You're wrong")
